import com.sun.deploy.cache.Cache;
import org.junit.Test;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class Main {
    WebDriver driver;
    JavascriptExecutor js;

    @BeforeClass
    public static void mainPreconditions() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
    }

    @Before
    public void preconditions() {
        driver = new ChromeDriver();
    }

    @Test
    public void todomvc() throws InterruptedException {
        driver.get("http://www.todomvc.com/examples/backbone/");
        driver.findElement(By.tagName("input")).sendKeys("Для успешной сдачи контрольной", Keys.ENTER);
        driver.findElement(By.tagName("input")).sendKeys("Надо повторить HTML + CSS", Keys.ENTER);
        driver.findElement(By.tagName("input")).sendKeys("Javascript + JAVA", Keys.ENTER);
        driver.findElement(By.tagName("input")).sendKeys("Postman + SoapUI", Keys.ENTER);
        driver.findElement(By.tagName("input")).sendKeys("А также сети, ДОМ и тому подобное", Keys.ENTER);
        Thread.sleep(5000);
        Assert.assertTrue(driver.findElement(By.cssSelector(".todo-list li:nth-child(1)")).getText().contains("Для успешной сдачи контрольной"));
        Assert.assertTrue(driver.findElement(By.cssSelector(".todo-list li:nth-child(2)")).getText().contains("Надо повторить HTML + CSS"));
        Assert.assertTrue(driver.findElement(By.cssSelector(".todo-list li:nth-child(3)")).getText().contains("Javascript + JAVA"));
        Assert.assertTrue(driver.findElement(By.cssSelector(".todo-list li:nth-child(4)")).getText().contains("Postman + SoapUI"));
        Assert.assertTrue(driver.findElement(By.cssSelector(".todo-list li:nth-child(5)")).getText().contains("А также сети, ДОМ и тому подобное"));

        java.util.List<WebElement> input = driver.findElements(By.tagName("button"));
        int size = input.size();
        System.out.println("Размер введенных задач = " + size);
        for (int i = 0; i < size; i++) {
            System.out.println(input.get(i));
        }}

        @After
        public void postConditions() {
            /*  js.executeScript("localStorage.clear()");*/// Почему-т с включенной строкой тест ругается и не выполняется
//        driver.quit();
        }
    }

